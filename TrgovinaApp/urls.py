# Django imports
from django.conf.urls import url
# Project imports
from . import views


app_name = 'TrgovinaApp'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^new-card/$', views.new_card, name='new_card'),
    url(r'^payment/$', views.payment, name='payment'),
    url(r'^history/$', views.history, name='history'),
]
