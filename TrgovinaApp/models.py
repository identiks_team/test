# encoding= utf-8

# Django imports
from django.db import models
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist


class Card(models.Model):
    card_number = models.PositiveIntegerField(
        unique=True,
        null=True,
        blank=False
    )
    value = models.FloatField(null=True, blank=False)
    available_amount = models.FloatField(null=True, blank=False)
    date_created = models.DateTimeField(
        null=True,
        auto_now_add=True,
        auto_now=False
    )

    # Get all Cards
    @staticmethod
    def get_cards():
        cards = Card.objects.all()
        return cards

    # Get next available Card number
    @staticmethod
    def get_next_number():
        cards = Card.objects.all()
        all_numbers = [0]
        for card in cards:
            all_numbers.append(card.card_number)
        return max(all_numbers) + 1

    # Card payment processing
    @staticmethod
    def payment(request, card_number, amount):
        try:
            # Get Card by card_number
            card = Card.objects.get(card_number=card_number)

            if amount <= card.available_amount:
                # Update available_amount on the Card
                new_available_amount = card.available_amount - amount
                card.available_amount = new_available_amount
                card.save()

                # Create consumption
                consumption = Consumption(card_id=card, consumption=amount)
                consumption.save()

                # Payment success message
                messages.success(
                    request,
                    'Success: Successful payment. ' +
                    'Check History for the history of all payments.')
            # Show warning if amount > card.available_amount
            else:
                messages.warning(
                    request,
                    'Warning: Available amount on Card #{0} is {1}€.'
                    .format(card.card_number,
                            format(card.available_amount, '.2f'))
                )
        # Show warning if Card with given card_number does not exist
        except ObjectDoesNotExist:
            messages.warning(request, 'Warning: This Card does not exist!')


class Shop(models.Model):
    shop_name = models.CharField(null=True, blank=False, max_length=256)


class Consumption(models.Model):
    card_id = models.ForeignKey(Card, null=True, blank=False)
    consumption = models.FloatField(null=True, blank=False)
    date_created = models.DateTimeField(
        null=True,
        auto_now_add=True,
        auto_now=False
    )

    # Get all payments
    @staticmethod
    def get_payments():
        payments = Consumption.objects.all()
        return payments
