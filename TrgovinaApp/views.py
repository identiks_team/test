# Django imports
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
# Project imports
from .models import Card, Consumption
from .forms import NewCardForm, PaymentForm


def index(request):
    return render(request, "index.html")


def new_card(request):
    # Get next available Card number
    next_number = Card.get_next_number()

    # Initialize form with next available Card number
    form = NewCardForm(
        request.POST or None,
        initial={'card_number': next_number}
    )

    # Get all Cards
    cards = Card.get_cards()

    if form.is_valid():
        # Get data from valid form
        card_number = form.cleaned_data['card_number']
        value = form.cleaned_data['value']
        # Create new Card
        new_card = Card(
            card_number=card_number,
            value=value,
            available_amount=value
        )
        new_card.save()

        return HttpResponseRedirect(reverse('new_card'))

    context = {
        'form': form,
        'cards': cards
    }

    return render(request, "new_card.html", context)


def payment(request):
    # Initialize payment form
    form = PaymentForm(request.POST or None)

    if form.is_valid():
        # Get data from valid form
        card_number = form.cleaned_data['card_number']
        amount = form.cleaned_data['amount']
        # Process the payment
        Card.payment(request, card_number, amount)
        return HttpResponseRedirect(reverse('payment'))
    elif form.errors:
        messages.warning(request, 'Warning: Check for errors in form.')

    context = {
        'form': form
    }

    return render(request, "payment.html", context)


def history(request):
    # Get all payments
    all_payments = Consumption.get_payments()

    context = {
        'all_payments': all_payments
    }

    return render(request, "history.html", context)
