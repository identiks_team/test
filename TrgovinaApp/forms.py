# Django imports
from django import forms
# Project imports
from .models import Card


# Model form for creating card
class NewCardForm(forms.ModelForm):
    class Meta:
        model = Card
        fields = ['card_number', 'value']


# Form for payment processing
class PaymentForm(forms.Form):
    card_number = forms.IntegerField()
    amount = forms.FloatField()
